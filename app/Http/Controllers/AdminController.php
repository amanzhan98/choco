<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Stock;

class AdminController extends Controller
{
	public function index(){
		$stocks = Stock::all();
		return view('index', compact(['stocks']));
	}
    public function AddReview(Request $req){
    	$review = new Review();
    	$review->comment = $req['comment'];
    	$review->grade = $req['grade'];
    	$review->stock_id = $req['stock_id'];
    	$review->save();
    	return back()->with('success', 'The review has been added');
    }
    protected function Offer($id){
    	$stocks = Stock::find($id);    	
        $reviews = Review::where('stock_id', $id)->get();
    	return view('offer1', compact(['reviews', 'stocks']));
    }
}
