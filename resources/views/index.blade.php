<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width" initial-scale=1, maximum-scale=1>
	
	<title>Катания на лыжах и сноуборде на абсолютно новой снежной трассе, скидка 30% - Алматы - Chocolife.me (39540)</title>

	<link rel="stylesheet" href="css/style.css">

</head>


<body>
<div class="header">
    <div class="wrapper header_wrapper">
        <div class="services">
            <a href="" class="services_item serv_i_chocolife"></a>
            <a href="" class="services_item serv_i_chocomart"></a>
            <a href="" class="services_item serv_i_chocotravel"></a>
            <a href="" class="services_item serv_i_lensmark"></a>
            <a href="" class="services_item serv_i_chocofood"></a>
            <a href="" class="services_item serv_i_idoctor"></a>
        </div>
        <div class="header_auth-cart">
            <div class="authorization">
                <a href="#" class="button button-reg">Регистрация</a>
                <a href="#" class="button button-enter">Вход</a>
            </div>
            
            <div class="cart">
                <div class="cart_icon"><img src="img/cart_icon.png" id="cart_icon" width=18px; height=16px;></div>
                <p class="cart_count" id="cart_count">0</p>
            </div>
        </div>
            
        
    </div>
</div>
<div class="wrapper top-detail">
    <div class="wrapper_top">
        <div class="cities">
            <!--TODO cities dropdown-->
            <div class="cities_almaty">
                Алматы
                <span class="caret"></span>
            </div>
        </div>
        <div class="inf">
            <a href="#" class="inf_option help">
                <div class="inf_icon inf_icon_bulb"></div>
                <p class="inf_content">нужна помощь?</p>
            </a>
            <a href="#" class="inf_option protect">
                <div class="inf_icon inf_icon_shield"></div>
                <p class="inf_content">защита покупателей</p>
            </a>
            <a href="#" class="inf_option msg">
                <div class="inf_icon inf_icon_message"></div>
                <p class="inf_content">обратная связь</p>
            </a>
        </div>
		<div class="header_logo-search">
			<div class="flex-center">
				<img src="img/logo.png" id="logo"  width=190px; height=35px; >
				<p class="slogan" id="slogan"> Главное, чтобы Вы были счастливы!</p> 
			</div>
			
			<form class="form search-form">
				<input type="text" placeholder="Найти среди 612 акций" class="input">
				<button type="submit" class="button button--type-submit button--yellow-block"></button>
			</form>
			
		</div>
    </div>
    
	<hr id="hr-top">
	<div class="categories categories--prevent-fixed-state">
    <div class="categories__item">Все</div>
    <div class="categories__item">Новые</div>
    <div class="categories__item red">Хиты продаж</div>
    <div class="categories__item">Развлечения и отдых</div>
    <div class="categories__item">Красота и здоровье</div>
    <div class="categories__item">Спорт</div>
    <div class="categories__item">Товары</div>
    <div class="categories__item">Услуг</div>
    <div class="categories__item">Еда</div>
    <div class="categories__item">Туризм, отели</div>
    <div class="categories__item green">Бесплатные купоны</div>
    <div class="categories__active-element"></div>
	
</div>
<hr id="hr-bottom">

    <div class="advertising">
        <a href="#" class="advertising_item advertising_item-first"></a>
        <a href="#" class="advertising_item advertising_item-second"></a>
        <a href="#" class="advertising_item advertising_item-third"></a>
        <a href="#" class="advertising_item advertising_item-forth"></a>
    </div>
	
	<form class="sorting">
        <p class="sorting_title">Сортировать: </p>
        <div class="sorting_option"></div>
        <label class="sorting_option">
            <input type="radio" name="sort" class="sorting__radio">
            <span class="sorting__fake-radio"></span>
            популярные
        </label>
        <label class="sorting_option">
            <input type="radio" name="sort" class="sorting__radio">
            <span class="sorting__fake-radio"></span>
            цена
        </label>
        <label class="sorting_option">
            <input type="radio" name="sort" class="sorting__radio">
            <span class="sorting__fake-radio"></span>
            скидка
        </label>
        <label class="sorting_option">
            <input type="radio" name="sort" class="sorting__radio">
            <span class="sorting__fake-radio"></span>
            новые
        </label>
        <label class="sorting_option">
            <input type="radio" name="sort" class="sorting__radio">
            <span class="sorting__fake-radio"></span>
            рейтинг
        </label>
    </form>
	
	<div class="offers" id="offers">
        @foreach($stocks as $stock)
        <a href="{{ route('Offer',$stock->id) }}" class="offers__item offers__item--shadow">
            <img src="img/{{$stock->image}}" alt="offer" class="offers__image">
        </a>
		@endforeach
    </div>
	
	<div class="seo-block info-block--gray">
        <div class="wrapper">
            <div>
                <img src="img/1.jpg" alt="">
                <h2 class="seo-block__title">Все скидки и акции в одном месте!</h2>
                <p>Человеку для счастья нужно совсем мало: оставаться здоровым, иметь крепкую семью и хорошую работу, хранить гармонию души. Но иногда это не все. Не хватает какого-то приятного дополнения, например, получить бешеную скидку на популярную <a href="#" class="blue">услугу</a>. И этот бонус можем подарить вам именно мы! Благодаря купонам, приобретенным в нашем сервисе, вы сможете стать самым счастливым человеком в любой точке города Алматы.</p>
                <p>Мы предлагаем скидки в Алматы на самые разнообразные виды деятельности и услуги, которые распределены по таким рубрикам:</p>
            </div>
                <ul class="seo-block__list">
                    <li class="seo-block__list-item">полная подборка (все самые актуальные);</li>
                    <li class="seo-block__list-item">новые (самые свежие предложения);</li>
                    <li class="seo-block__list-item">санатории (отдых и лечение в наиболее престижных санаториях);</li>
                    <li class="seo-block__list-item"><a class="blue" href="#">красота</a> (услуги салонов красоты, а также отдельных мастеров);</li>
                    <li class="seo-block__list-item">здоровье и спорт (услуги спортзалов, фитнес-клубов и т. д.);</li>
                    <li class="seo-block__list-item">еда (рестораны, кондитерские, суши, доставка и т. д.);</li>
                    <li class="seo-block__list-item">развлечения (караоке, сауна и т. д.);</li>
                    <li class="seo-block__list-item">услуги (авто-услуги, свадебные услуги и т. д.);</li>
                    <li class="seo-block__list-item">товары (для школы, для дома и т. д.);</li>
                    <li class="seo-block__list-item">отдых (туризм, гостиницы, отели и т. д.).</li>
                </ul>
            <h2 class="seo-block__title">Покупайте купоны и экономьте на услугах и товарах!</h2>
            <p>В нашем купонном сервисе Шоколайф можно приобрести горящие скидки и купоны в Алматы с самым разным акционным диапазоном: от 30% до 90%. На сайте представлено более 530 разных возможностей для реализации личных планов. Например, вы можете в очередной раз посетить «Sarafan cafe», но теперь со скидочным купоном на 50% оставить там намного меньшую сумму. Такой купон обойдется вам всего в 399 тенге.</p>
            <p>Срок действия каждой акции в Алматы указан на нашем сайте. Эта информация поможет приобрести купон для посещения вами не только интересного предложения, но и оптимального во времени. Чтобы быстрее отыскать нужную акцию в нашем агрегаторе скидок, воспользуйтесь удобным поиском сайта онлайн по всем имеющимся рубрикам.</p>
            <p>Отныне и навсегда сайт акций и скидок в Алматы должен стать вашим лучшим другом и помощником! С нашей помощью вы сэкономите уйму денежных средств, невозвратимое время и дорогое здоровье. Удобное оформление сайта, правильно подобранная цветовая гамма, удобство в расположении рубрик не оставят равнодушным ни молодежь, ни людей преклонного возраста.</p>
            <p>Скидочные купоны в Алматы, представленные у нас — это гарантия настоящего качества. Мы ручаемся за каждого своего компаньона, уверены в безупречности каждого предложения, ведь все проверено лично нами! Если вы не успели применить скидку по назначению, или вас не устроило обслуживание — звоните по телефону, указанному на сайте, и наша служба заботы о пользователях обязательно даст вам все разъяснения. При возникновении вопросов или предложений также можно связаться с нами через форму обратной связи.</p>
        </div>
    </div>
	
	    <div class="footer">
            <div class="wrapper footer-space-between footer--upper">
                <div class="footer-columns">
                    <div class="footer-columns__item" id="company">
                        <div class="footer-columns__heading" >Компания</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">О Chocolife.me</li>
                            <li class="footer-columns__list-item">Пресса о нас</li>
                            <li class="footer-columns__list-item">Контакты</li>
                        </ul>
                    </div>
                    <div class="footer-columns__item" id="client">
                        <div class="footer-columns__heading" >Клиентам</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Обратная связь</li>
                            <li class="footer-columns__list-item">Обучающий видеоролик</li>
                            <li class="footer-columns__list-item">Вопросы и ответы</li>
                            <li class="footer-columns__list-item">Публичная оферта</li>
                        </ul>
                    </div>
                    <div class="footer-columns__item" id="partner">
                        <div class="footer-columns__heading" >Партнерам</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Для Вашего бизнеса</li>
                        </ul>
                    </div>
                </div>
                <div class="footer-columns" id="suggest">
                    <div class="footer-columns__item info-columns__item--apps" >
                        <div class="footer-columns__heading">Наше приложение</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item" >Chocolife.me теперь еще удобнее и всегда под рукой!</li>
                        </ul>
                        <div class="mobile-apps">
                            <a href="#" class="mobile-apps__item mobile-apps__item--google-play"></a>
                            <a href="#" class="mobile-apps__item mobile-apps__item--app-store"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper footer--space-between">
                <div class="footer__text" >Chocolife.me | 2011-2018  -  <a href="#">Карта сайта</a></div>
                <div class="social">
                    <p>Chocolife.me в социальных сетях:</p>
                    <a href="#" class="social__icon social__icon--vk"></a>
                    <a href="#" class="social__icon social__icon--facebook"></a>
                    <a href="#" class="social__icon social__icon--youtube"></a>
                    <a href="#" class="social__icon social__icon--instagram"></a>
                </div>
            </div>
    </div>


</body>

</html>