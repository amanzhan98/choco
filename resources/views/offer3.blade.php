<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width" initial-scale=1, maximum-scale=1>
	
	<title>Артисты Большого Московского цирка и Цирка Никулина ждут вас 4 февраля, скидка 30% - Алматы - Chocolife.me (39969)</title>

	<link rel="stylesheet" href="css/style.css">

</head>


<body>
<div class="header">
    <div class="wrapper header_wrapper">
        <div class="services">
            <a href="" class="services_item serv_i_chocolife"></a>
            <a href="" class="services_item serv_i_chocomart"></a>
            <a href="" class="services_item serv_i_chocotravel"></a>
            <a href="" class="services_item serv_i_lensmark"></a>
            <a href="" class="services_item serv_i_chocofood"></a>
            <a href="" class="services_item serv_i_idoctor"></a>
        </div>
        <div class="header_auth-cart">
            <div class="authorization">
                <a href="#" class="button button-reg">Регистрация</a>
                <a href="#" class="button button-enter">Вход</a>
            </div>
            
            <div class="cart">
                <div class="cart_icon"><img src="img/cart_icon.png" id="cart_icon" width=18px; height=16px;></div>
                <p class="cart_count" id="cart_count">0</p>
            </div>
        </div>
            
        
    </div>
</div>
<div class="wrapper top-detail">
    <div class="wrapper_top">
        <div class="cities">
            <!--TODO cities dropdown-->
            <div class="cities_almaty">
                Алматы
                <span class="caret"></span>
            </div>
        </div>
        <div class="inf">
            <a href="#" class="inf_option help">
                <div class="inf_icon inf_icon_bulb"></div>
                <p class="inf_content">нужна помощь?</p>
            </a>
            <a href="#" class="inf_option protect">
                <div class="inf_icon inf_icon_shield"></div>
                <p class="inf_content">защита покупателей</p>
            </a>
            <a href="#" class="inf_option msg">
                <div class="inf_icon inf_icon_message"></div>
                <p class="inf_content">обратная связь</p>
            </a>
        </div>
		<div class="header_logo-search">
			<div class="flex-center">
				<img src="img/logo.png" id="logo"  width=190px; height=35px; >
				<p class="slogan" id="slogan"> Главное, чтобы Вы были счастливы!</p> 
			</div>
			
			<form class="form search-form">
				<input type="text" placeholder="Найти среди 664 акций" class="input">
				<button type="submit" class="button button--type-submit button--yellow-block"></button>
			</form>
			
		</div>
    </div>
    
	<hr id="hr-top">
	<div class="categories categories--prevent-fixed-state">
    <div class="categories__item">Все</div>
    <div class="categories__item">Новые</div>
    <div class="categories__item red">Хиты продаж</div>
    <div class="categories__item">Развлечения и отдых</div>
    <div class="categories__item">Красота и здоровье</div>
    <div class="categories__item">Спорт</div>
    <div class="categories__item">Товары</div>
    <div class="categories__item">Услуг</div>
    <div class="categories__item">Еда</div>
    <div class="categories__item">Туризм, отели</div>
    <div class="categories__item green">Бесплатные купоны</div>
    <div class="categories__active-element"></div>
	
</div>
<hr id="hr-bottom">

<div class="wrapper">
    <div class="first-offer">
        <div class="first-offer__heading">
            <span>Можно купить с 25 января по 31 января</span>
            <span>Можно воспользоваться до 4 февраля 2018 года</span>
        </div>
        <div class="first-offer__content">
            <h2 class="first-offer__title">Цирк! Цирк! Цирк! Артисты «Большого Московского цирка» и «Цирка Никулина» с новой зажигательной программой ждут вас 4 февраля! Билеты со скидкой 30%!</h2>
            <div class="first-offer__content-info">
                <div class="slider">
                    <img class="cirk" src="img/carnelly.jpg" height=300px width=650px  alt="">
                </div>
                <div class="offer-info">
                    
                    <button class="button buy" >Купить</button>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="first-offer-text tabs">
        <ul class="first-offer-text__menu">
            <li class="first-offer-text__menu-item first-offer-text__menu-item--active tabs__button" data-tab-for="0">Информация</li>
            <li class="first-offer-text__menu-item tabs__button" data-tab-for="1">Отзывы</li>
            <li class="first-offer-text__menu-item tabs__button" data-tab-for="2">Вопросы</li>
            <li class="first-offer-text__menu-item">Получить 5000 тенге</li>
            <li class="first-offer-text__menu-item"><a href="#" class="light-blue">Как воспользоваться акцией?</a></li>
        </ul>
        <div class="first-offer-text__template">
            <div class="tabs__item" data-tab-index="0">
                <div class="first-offer-info">
                    <div class="first-offer-info__side">
                        <a href="#" class="guard">
                            <span class="guard-icon guard-icon--extend"></span>
                            Расширенная защита покупателей</a>
                        <ul class="first-offer-info__list">
                            <li class="first-offer-info__list-heading">Условия:</li>
                            <li class="first-offer-info__list-item">Сертификат предоставляет возможность посетить Казахский Государственный Цирк.</li>
                            <li class="first-offer-info__list-item">Выбор мест не предоставляется.</li>
                            <li class="first-offer-info__list-item">Билеты выдаются в порядке очередности.</li>
                            <li class="first-offer-info__list-item"><strong>Дата и время проведения:</strong> 4 февраля, в 16:00.</li>
                            <li class="first-offer-info__list-item"><strong>Адрес проведения:</strong> г. Алматы, пр. Абая, 50, «Казахский Государственный Цирк».</li>
                            <li class="first-offer-info__list-item"><strong>Сертификат необходимо обменять на билеты в офисе Сhocolife.me до 3 февраля (включительно)</strong>по адресу: г. Алматы, ул. Байзакова, 280, БЦ Almaty Towers, офис 222.</li>
                            <li class="first-offer-info__list-item">Скидка не суммируется с другими действующими предложениями цирка.</li>
                            <li class="first-offer-info__list-item">Дети до 4 лет могут посещать цирк бесплатно (без предоставления места)</li>
                            <li class="first-offer-info__list-item">Ограничений по возрасту нет. <br> <br> Согласно внутренним правилам цирка, приносить с собой еду не разрешается.</li>
                            <li class="first-offer-info__list-item">После приобретения сертификата дополнительно оплачивать ничего не нужно.</li>
                            <li class="first-offer-info__list-item"><strong>Справки по телефону:</strong><br>+7 (727) 346-85-88 (офис Chocolife.me).</li>
                            <li class="first-offer-info__list-item">Вы можете приобрести неограниченное количество сертификатов по данной акции как для себя, так и в подарок.</li>
                            <li class="first-offer-info__list-item"><strong>Сертификат распечатывать необязательно, достаточно сообщить его номер и SC-код.</strong></li>
                            <li class="first-offer-info__list-item">Сертификат действителен до 4 февраля 2018 г. (включительно).</li>
                            <li class="first-offer-info__list-item"><a href="#" class="link--light-blue">Политика по возврату средств</a></li>
                            <li class="first-offer-info__list-item"><a href="#" class="link--light-blue">Стандартные условия каждой акции</a></li>
                        </ul>
                        <ul class="first-offer-info__list">
                            <li class="first-offer-info__list-heading">Адрес:</li>
                            <li class="first-offer-info__list-item">Алматинская обл., Талгарский район, садоводческое общество, ул. Мичурина, 4, «Бескайнар», база отдыха «Табаган»
                                <a href="" class="link--light-blue link--dashed">Посмотреть на карте</a></li>
                            <li class="first-offer-info__list-item">График работы: <br> Ежедневно: c 09:00 до 22:00 </li>
                        </ul>
                        <ul class="first-offer-info__list">
                            <li class="first-offer-info__list-heading">Отзывы:</li>
                            @foreach($reviews as $review)
                            <li class="first-offer-info__list-item">{{$review->comment}}</li>
                            <li class="first-offer-info__list-item">Оценка:&nbsp;{{$review->grade}} </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="first-offer-info__side">
                        <div class="first-offer-info__map">
                            <img src="img/map1.png" alt="">
                        </div>
                        <div class="first-offer-info__video">
                            <iframe src="https://www.youtube.com/embed/p8W8WsCm-gU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <ul class="first-offer-info__list">
                            <li class="first-offer-info__list-heading" id="features">Особенности:</li>
                            <li class="first-offer-info__list-item">
                                Артисты «Большого Московского цирка» и «Цирка Никулина» лауреаты и призеры различных мировых фестивалей циркового искусство представляют:
                                <ul class="first-offer-info__list">
                                    <li class="first-offer-info__list-item">акробаты на подкидной доске;</li>
                                    <li class="first-offer-info__list-item">воздушные гимнасты;</li>
                                    <li class="first-offer-info__list-item">лапундеры (обезьяны);</li>
                                    <li class="first-offer-info__list-item">нубийские козлики;</li>
                                    <li class="first-offer-info__list-item">мини лошадка;</li>
                                    <li class="first-offer-info__list-item">конное трио (высшая школа верховой езды);</li>
                                    <li class="first-offer-info__list-item">собаки;</li>
                                    <li class="first-offer-info__list-item">и конечно же клоуны.</li>
                                </ul>
                            </li>
                            <li class="first-offer-info__list-item">Длительность — 2,5 часа.</li>
                            <li class="first-offer-info__list-item">
                                Артисты
                                <ul class="first-offer-info__list">
                                    <li class="first-offer-info__list-item">Ковгар Андрей;</li>
                                    <li class="first-offer-info__list-item">Кох-Кукис Мария;</li>
                                    <li class="first-offer-info__list-item">Гукаев Константин.</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
           <!--  @if (Session::has('success'))
                <div class="success">
                    {{ Session::get('success') }}
                </div>
            @else
            <div class="review">
                <div class="first-offer-reviews">
                    <div class="review" >
                        <form action="{{route('AddReview')}}" method="post">
                            {{csrf_field()}}
                            <textarea name="comment" id="" cols="30" rows="10" required></textarea>
                            <select name="grade" id="" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <input type="submit">
                        </form>
                    </div>
                </div>
            </div>
            @endif -->
        </div>
        <div class="first-offer-text__footer">
            <div class="first-offer-text__payment-button">
                <div class="payment">
                    <span>Способы оплаты:  </span>
                    <div class="payment__item payment__item--visa"></div>
                    <div class="payment__item payment__item--mcard"></div>
                    <div class="payment__item payment__item--qiwi"></div>
                </div>
                <button class="button buy">Купить</button>
            </div>
            
        </div>
    </div>
    <p>Если Вам встретилась ошибка в тексте, выделите её левой кнопкой мыши и нажмите Ctrl+Enter</p>
</div>

<div class="footer">
            <div class="wrapper footer-space-between footer--upper">
                <div class="footer-columns">
                    <div class="footer-columns__item" id="company">
                        <div class="footer-columns__heading" >Компания</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">О Chocolife.me</li>
                            <li class="footer-columns__list-item">Пресса о нас</li>
                            <li class="footer-columns__list-item">Контакты</li>
                        </ul>
                    </div>
                    <div class="footer-columns__item" id="client">
                        <div class="footer-columns__heading" >Клиентам</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Обратная связь</li>
                            <li class="footer-columns__list-item">Обучающий видеоролик</li>
                            <li class="footer-columns__list-item">Вопросы и ответы</li>
                            <li class="footer-columns__list-item">Публичная оферта</li>
                        </ul>
                    </div>
                    <div class="footer-columns__item" id="partner">
                        <div class="footer-columns__heading" >Партнерам</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Для Вашего бизнеса</li>
                        </ul>
                    </div>
                </div>
                <div class="footer-columns" id="suggest">
                    <div class="footer-columns__item info-columns__item--apps" >
                        <div class="footer-columns__heading">Наше приложение</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Chocolife.me теперь еще удобнее и всегда под рукой!</li>
                        </ul>
                        <div class="mobile-apps">
                            <a href="#" class="mobile-apps__item mobile-apps__item--google-play"></a>
                            <a href="#" class="mobile-apps__item mobile-apps__item--app-store"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper footer--space-between">
                <div class="footer__text">Chocolife.me | 2011-2018  -  <a href="#">Карта сайта</a></div>
                <div class="social">
                    <p>Chocolife.me в социальных сетях:</p>
                    <a href="#" class="social__icon social__icon--vk"></a>
                    <a href="#" class="social__icon social__icon--facebook"></a>
                    <a href="#" class="social__icon social__icon--youtube"></a>
                    <a href="#" class="social__icon social__icon--instagram"></a>
                </div>
            </div>
</div>

</body>

</html>