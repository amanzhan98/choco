<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width" initial-scale=1, maximum-scale=1>
	
	<title>Катания на лыжах и сноуборде на абсолютно новой снежной трассе, скидка 30% - Алматы - Chocolife.me (39540)</title>

	<link rel="stylesheet" href="../css/style.css">

</head>


<body>
<div class="header">
    <div class="wrapper header_wrapper">
        <div class="services">
            <a href="" class="services_item serv_i_chocolife"></a>
            <a href="" class="services_item serv_i_chocomart"></a>
            <a href="" class="services_item serv_i_chocotravel"></a>
            <a href="" class="services_item serv_i_lensmark"></a>
            <a href="" class="services_item serv_i_chocofood"></a>
            <a href="" class="services_item serv_i_idoctor"></a>
        </div>
        <div class="header_auth-cart">
            <div class="authorization">
                <a href="#" class="button button-reg">Регистрация</a>
                <a href="#" class="button button-enter">Вход</a>
            </div>
            
            <div class="cart">
                <div class="cart_icon"><img src="../img/cart_icon.png" id="cart_icon" width=18px; height=16px;></div>
                <p class="cart_count" id="cart_count">0</p>
            </div>
        </div>
            
        
    </div>
</div>
<div class="wrapper top-detail">
    <div class="wrapper_top">
        <div class="cities">
            <!--TODO cities dropdown-->
            <div class="cities_almaty">
                Алматы
                <span class="caret"></span>
            </div>
        </div>
        <div class="inf">
            <a href="#" class="inf_option help">
                <div class="inf_icon inf_icon_bulb"></div>
                <p class="inf_content">нужна помощь?</p>
            </a>
            <a href="#" class="inf_option protect">
                <div class="inf_icon inf_icon_shield"></div>
                <p class="inf_content">защита покупателей</p>
            </a>
            <a href="#" class="inf_option msg">
                <div class="inf_icon inf_icon_message"></div>
                <p class="inf_content">обратная связь</p>
            </a>
        </div>
		<div class="header_logo-search">
			<div class="flex-center">
				<img src="../img/logo.png" id="logo"  width=190px; height=35px; >
				<p class="slogan" id="slogan"> Главное, чтобы Вы были счастливы!</p> 
			</div>
			
			<form class="form search-form">
				<input type="text" placeholder="Найти среди 664 акций" class="input">
				<button type="submit" class="button button--type-submit button--yellow-block"></button>
			</form>
			
		</div>
    </div>
    
	<hr id="hr-top">
	<div class="categories categories--prevent-fixed-state">
    <div class="categories__item">Все</div>
    <div class="categories__item">Новые</div>
    <div class="categories__item red">Хиты продаж</div>
    <div class="categories__item">Развлечения и отдых</div>
    <div class="categories__item">Красота и здоровье</div>
    <div class="categories__item">Спорт</div>
    <div class="categories__item">Товары</div>
    <div class="categories__item">Услуг</div>
    <div class="categories__item">Еда</div>
    <div class="categories__item">Туризм, отели</div>
    <div class="categories__item green">Бесплатные купоны</div>
    <div class="categories__active-element"></div>
	
</div>
<hr id="hr-bottom">

<div class="wrapper">
    <div class="first-offer">
        <div class="first-offer__heading">
            <span>Можно купить с 22 декабря по 1 февраля</span>
            <span>Можно воспользоваться до 28 февраля 2018 года</span>
        </div>
        <div class="first-offer__content">
            <h2 class="first-offer__title">{{$stocks->title}}</h2>
            <div class="first-offer__content-info">
                <div class="slider">
                    <img class="slider" src="../img/{{$stocks->image}}" height=300px width=650px alt="">
                </div>
                <div class="offer-info">
                    
                    <button class="button buy" id="f_buy">Купить</button>
                    
                </div>
            </div>
        </div>        
    </div>
</div>
@foreach($reviews as $review)
            <div class="review">
                <p>{{$review->comment}}</p>
                <p>Оценка {{$review->grade}}</p>
            </div>
        @endforeach
        @if (Session::has('success'))
            <div class="success">
                {{ Session::get('success') }}
            </div>
        @else
        <div class="review">
            <div class="first-offer-reviews">
                <div class="review" >
                    <form action="{{route('AddReview')}}" method="post">
                        {{csrf_field()}}
                        <input type="text" name="stock_id" value="{{$stocks->id}}" hidden>
                        <textarea name="comment" id="" cols="30" rows="10" required></textarea>
                        <label>Grade: </label>
                        <select name="grade" id="" required>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <input type="submit">
                    </form>
                </div>
            </div>
        </div>
        @endif

<div class="footer">
            <div class="wrapper footer-space-between footer--upper">
                <div class="footer-columns">
                    <div class="footer-columns__item" id="company">
                        <div class="footer-columns__heading" >Компания</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">О Chocolife.me</li>
                            <li class="footer-columns__list-item">Пресса о нас</li>
                            <li class="footer-columns__list-item">Контакты</li>
                        </ul>
                    </div>
                    <div class="footer-columns__item" id="client">
                        <div class="footer-columns__heading" >Клиентам</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Обратная связь</li>
                            <li class="footer-columns__list-item">Обучающий видеоролик</li>
                            <li class="footer-columns__list-item">Вопросы и ответы</li>
                            <li class="footer-columns__list-item">Публичная оферта</li>
                        </ul>
                    </div>
                    <div class="footer-columns__item" id="partner">
                        <div class="footer-columns__heading" >Партнерам</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Для Вашего бизнеса</li>
                        </ul>
                    </div>
                </div>
                <div class="footer-columns" id="suggest">
                    <div class="footer-columns__item info-columns__item--apps" >
                        <div class="footer-columns__heading">Наше приложение</div>
                        <ul class="footer-columns__list">
                            <li class="footer-columns__list-item">Chocolife.me теперь еще удобнее и всегда под рукой!</li>
                        </ul>
                        <div class="mobile-apps">
                            <a href="#" class="mobile-apps__item mobile-apps__item--google-play"></a>
                            <a href="#" class="mobile-apps__item mobile-apps__item--app-store"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper footer--space-between">
                <div class="footer__text">Chocolife.me | 2011-2018  -  <a href="#">Карта сайта</a></div>
                <div class="social">
                    <p>Chocolife.me в социальных сетях:</p>
                    <a href="#" class="social__icon social__icon--vk"></a>
                    <a href="#" class="social__icon social__icon--facebook"></a>
                    <a href="#" class="social__icon social__icon--youtube"></a>
                    <a href="#" class="social__icon social__icon--instagram"></a>
                </div>
            </div>
</div>
</body>

</html>